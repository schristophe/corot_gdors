import numpy as np
import pickle
from astropy.table import Table
from astropy.timeseries import LombScargle

import src.frequency_analysis.extract


def save_all(filename, lc, model, freqlist):
    """"""
    _save_lc(filename, lc)
    _save_lsper(filename, lc)
    _save_spectral_window(filename, lc)
    if freqlist.size > 0 and model is not None:
        _save_lc_residuals(filename, lc, model)
        _save_lsper_residuals(filename, lc, model)
        _save_freqlist(filename, freqlist)
        _save_combination_list(filename, freqlist)


def _save_lc(filename, lc):
    """ """
    lc_table = Table(
        lc[:, :2], names=("time", "normalized_flux"), units=["BJD-2400000", ""]
    )
    lc_table.write(
        f"data/processed/{filename}",
        format="hdf5",
        path="lc",
        overwrite=True,
        append=True,
        serialize_meta=True,
    )


def _save_lc_residuals(filename, lc, model):
    """ """
    lc_copy = lc.copy()
    lc_copy[:, 1] = -model.residual  # data - model
    lc_table = Table(
        lc_copy[:, :2],
        names=("time", "normalized_flux"),
        units=["BJD-2400000", "residuals"],
    )
    lc_table.write(
        f"data/processed/{filename}",
        format="hdf5",
        path="lc_residuals",
        overwrite=True,
        append=True,
        serialize_meta=True,
    )


def _save_lsper(filename, lc):
    """ """
    ls = LombScargle(lc[:, 0], lc[:, 1], normalization="psd").autopower(
        samples_per_peak=20, nyquist_factor=1
    )
    ls = Table(ls, names=("frequency", "amplitude"), units=["c/d", ""])
    ls["amplitude"] = 2 * np.sqrt(ls["amplitude"] / len(lc[:, 0]))
    ls.write(
        f"data/processed/{filename}",
        format="hdf5",
        path="lsper",
        overwrite=True,
        append=True,
        serialize_meta=True,
    )


def _save_lsper_residuals(filename, lc, model):
    """ """
    ls = LombScargle(lc[:, 0], model.residual, normalization="psd").autopower(
        samples_per_peak=20, nyquist_factor=1
    )
    ls = Table(ls, names=("frequency", "amplitude"), units=["c/d", ""])
    ls["amplitude"] = 2 * np.sqrt(ls["amplitude"] / len(lc[:, 0]))
    ls.write(
        f"data/processed/{filename}",
        format="hdf5",
        path="lsper_residuals",
        overwrite=True,
        append=True,
        serialize_meta=True,
    )


def _save_spectral_window(filename, lc):
    """ """
    spectral_window = src.frequency_analysis.extract.get_spectral_win(lc)
    spectral_window = Table(
        spectral_window, names=("frequency", "amplitude"), units=["c/d", ""]
    )

    spectral_window.write(
        f"data/processed/{filename}",
        format="hdf5",
        path="spectral_window",
        overwrite=True,
        append=True,
        serialize_meta=True,
    )


def _save_freqlist(filename, freqlist):
    """ """
    names = (
        "freq_id",
        "amplitude",
        "frequency",
        "phase",
        "snr",
        "err_amp",
        "err_freq",
        "err_phase",
        "amp_criterion",
    )
    units = ["", "", "c/d", "rad/(2pi)", "", "", "c/d", "rad/(2pi)", ""]
    dtype = [int] + [np.float64] * (len(names) - 1)
    freqlist = Table(freqlist, names=names, units=units, dtype=dtype)
    freqlist.write(
        f"data/processed/{filename}",
        format="hdf5",
        path="freqlist",
        overwrite=True,
        append=True,
        serialize_meta=True,
    )


def _save_combination_list(filename, freqlist, nparents=20):
    """"""
    combination_list = src.frequency_analysis.extract.identify_combinations(
        freqlist, nparents=nparents
    )
    if combination_list.size == 0:
        _save_empty_table(filename, "combination_list")
    names = ["freq_id", "frequency_value", "combination_value"] + [
        f"F{i:.0f}" for i in freqlist[:nparents, 0]
    ]
    units = ["", "c/d", "c/d"] + [""] * len(freqlist[:nparents, 0])
    dtype = [int, np.float64, np.float64] + [int] * len(freqlist[:nparents, 0])
    combination_list = Table(combination_list, names=names, units=units, dtype=dtype)
    combination_list.write(
        f"data/processed/{filename}",
        format="hdf5",
        path="combination_list",
        overwrite=True,
        append=True,
        serialize_meta=True,
    )


def _save_empty_table(filename, path):
    empty_table = Table()
    empty_table.write(
        f"data/processed/{filename}",
        format="hdf5",
        path=path,
        overwrite=True,
        append=True,
        serialize_meta=True,
    )
