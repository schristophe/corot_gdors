import sys
from itertools import combinations, product
from textwrap import indent

import matplotlib.pyplot as plt
import numpy as np
import pickle
import yaml
from astropy.timeseries import LombScargle
from lmfit import Parameters, minimize, fit_report
from scipy.optimize import curve_fit

from src.utils import Logger

SNR_LIMIT = 4.0  # stop frequency extraction at this limit
RES_FACTOR = 2.0  # frequencies are unresolved if difference is < RES_FACTOR / T
WINDOW_LENGTH = 1.0  # in unit of measurement
NFREQ_MAX = 100  # max number of frequencies fitted to the lightcurve
EPS_COMBI = 2e-4  # threshold tolerance for combination frequencies
WARN_AMP_CRIT = 0.85  # warn in log if amp criterion is under this value


def extract_freqs_with_curve_fit(lc):
    """DO NOT USE IT. Extract a list of frequencies from a lightcurve.

    It is assumed that the lightcurve is already detrended and that the outliers
    were removed.
    """
    # Compute general characteristics of the lc
    fres = 1 / (lc[-1, 0] - lc[0, 0])
    fnyq = 1 / (2 * np.median(np.diff(lc[:, 0])))
    # Step 1: Prewhiten the lc by iteratively fitting a sinewave (linearly)
    print("Step 1: Prewhiten the lc by iteratively fitting a sinewave\n")
    lc_copy = lc.copy()
    freqlist = []
    nfreq = 1
    while nfreq < NFREQ_MAX:

        # Compute the LS periodogram
        ls = LombScargle(lc_copy[:, 0], lc_copy[:, 1], normalization="psd")
        freq, power = ls.autopower(samples_per_peak=10, nyquist_factor=1)
        offset = ls.offset()
        lc_copy[:, 1] -= offset

        # Harmonic fit using the frequency with highest amplitude
        # Get sensible initial guess
        i_max_power = np.argmax(power)
        fpeak = freq[i_max_power]
        amp0 = 2 * np.sqrt(power[i_max_power] / len(lc_copy[:, 0]))
        phase0, _ = curve_fit(
            lambda x, phase: _sine_func(x, amp0, fpeak, phase),
            lc_copy[:, 0],
            lc_copy[:, 1],
            p0=0.5,
            bounds=(0, 1),
        )

        p0 = (amp0, fpeak, phase0[0])

        bounds = [
            (0.5 * amp0, fpeak - 2 * fres, 0),
            (2 * amp0, fpeak + 2 * fres, 1),
        ]
        popt, pcov = curve_fit(
            _sine_func,
            lc_copy[:, 0],
            lc_copy[:, 1],
            p0=p0,
            bounds=bounds,
            check_finite=True,
            maxfev=10000,
        )
        # Compute the SNR for the fitted frequency
        max_power = power[i_max_power]
        mask_window = (freq >= fpeak - 0.5 * WINDOW_LENGTH) & (
            freq <= fpeak + 0.5 * WINDOW_LENGTH
        )
        snr = np.sqrt(max_power) / np.mean(np.sqrt(power[mask_window]))
        if snr < SNR_LIMIT:
            print(popt, snr)
            break

        plt.figure(figsize=(12, 6))
        plt.plot(lc_copy[:, 0], lc_copy[:, 1])
        plt.plot(lc_copy[:, 0], _sine_func(lc_copy[:, 0], *popt))
        plt.xlim(54400, 54460)
        plt.savefig(f"lc_f{nfreq}.png")

        plt.close()
        print(nfreq, *popt, snr)
        # Substract the fit
        lc_copy[:, 1] -= _sine_func(lc_copy[:, 0], *popt)

        freqlist.append([nfreq, *popt, snr])
        plt.figure(figsize=(12, 6))
        plt.plot(freq, power)
        plt.axvline(popt[1], 0.98, 1, c="k")
        plt.xlim(0, 5.5)
        plt.savefig(f"periodogram_f{nfreq}.png")
        plt.close()

        nfreq += 1
    freqlist = np.array(freqlist)

    # Step 2: Discard unresolved peaks (a peak is considered unresolved if it
    # is closer than 2.5xT^-1 of another peak having a higher amplitude).
    print("Step 2: Discard unresolved peaks\n")
    freqlist = discard_unresolved_peaks(freqlist, fres)

    # Step 3: Perform a global non-linear fit including all frequencies
    print("Step 3: Perform a global non-linear fit including all frequencies\n")
    # that pass Step 2.
    nfreq = len(freqlist)
    popt, pcov = fit_lc_model(lc, freqlist, offset)

    # Step 4: Compute the amplitude criterion for all kept frequencies
    print("Step 4: Compute the amplitude criterion for all kept frequencies")
    amp_criterion = get_amp_criterion(lc, freqlist)

    popt = popt[1:].reshape(nfreq, 3)
    pstd = np.sqrt(np.diag(pcov))[1:].reshape(nfreq, 3)
    freqlist_ = np.hstack(
        (
            np.arange(1, nfreq + 1)[:, np.newaxis],
            popt,
            pstd,
            freqlist[:, -1][:, np.newaxis],  # signal-to-noise ratio
            amp_criterion[:, np.newaxis],
        )
    )

    # Step 5: Identify possible combination frequencies

    return freqlist, freqlist_


def extract_freqs(lc, log=None, pickle_model=None):
    """Extract a list of frequencies from a lightcurve.

    It is assumed that the lightcurve is already detrended and that the outliers
    were removed.
    """
    # Set up log
    logger = Logger(log)
    # Check if data contains nans

    # Compute general characteristics of the lc
    fres = 1 / (lc[-1, 0] - lc[0, 0])
    fnyq = 1 / (2 * np.median(np.diff(lc[:, 0])))

    # Step 1: Prewhiten the lc by iteratively fitting a sinewave
    logger.write("\n>> Step 1: Prewhiten the lc by iteratively fitting a sinewave\n")

    lc_copy = lc.copy()
    lc_copy[:, 1] -= np.mean(lc_copy[:, 1])

    freqlist = []
    nfreq = 0

    while nfreq < NFREQ_MAX:

        # Compute the LS periodogram
        ls = LombScargle(lc_copy[:, 0], lc_copy[:, 1], normalization="psd")
        freq, power = ls.autopower(samples_per_peak=20, nyquist_factor=1)

        # Harmonic fit using the frequency with highest amplitude
        # Get sensible initial guess
        i_max_power = np.argmax(power)
        fpeak = freq[i_max_power]
        amp0 = 2 * np.sqrt(power[i_max_power] / len(lc_copy[:, 0]))
        phase0, _ = curve_fit(
            lambda x, phase: _sine_func(x, amp0, fpeak, phase),
            lc_copy[:, 0],
            lc_copy[:, 1],
            p0=0.5,
            bounds=(0, 1),
        )

        params = Parameters()
        params.add("amp", value=amp0, min=0.5 * amp0, max=3 * amp0)
        params.add("freq", value=fpeak, min=fpeak - 2 * fres, max=fpeak + 2 * fres)
        params.add("phase", value=0.5, min=0, max=1)
        out = minimize(
            _residual, params, args=(lc_copy[:, 0],), kws={"data": lc_copy[:, 1]}
        )

        # Compute the SNR for the fitted frequency
        max_power = power[i_max_power]
        mask_window = (freq >= fpeak - 0.5 * WINDOW_LENGTH) & (
            freq <= fpeak + 0.5 * WINDOW_LENGTH
        )
        snr = np.sqrt(max_power) / np.mean(np.sqrt(power[mask_window]))
        if snr < SNR_LIMIT:
            break

        nfreq += 1

        # Substract the fit
        lc_copy[:, 1] -= _residual(out.params, lc_copy[:, 0])

        freqlist.append(
            [nfreq]
            + [
                out.params["amp"].value,
                out.params["freq"].value,
                out.params["phase"].value,
            ]
            + [snr]
        )
        logger.write(
            nfreq,
            out.params["amp"].value,
            out.params["freq"].value,
            out.params["phase"].value,
            snr,
        )

    freqlist = np.array(freqlist)

    if nfreq == 0:
        logger.write("No significant frequency could be extracted.\n")
        return freqlist, None

    # Step 2: Discard unresolved peaks (a peak is considered unresolved if it
    # is closer than RES_FACTORxT^-1 of another peak having a higher amplitude).
    logger.write("\n>> Step 2: Discard unresolved peaks\n")

    freqlist = discard_unresolved_peaks(freqlist, fres, logger)

    nfreq = len(freqlist)

    # Step 3: Perform a global non-linear fit including all frequencies
    # that pass Step 2.
    logger.write(
        "\n>> Step 3: Perform a global non-linear fit including all frequencies\n"
    )

    all_params = Parameters()
    for i, (amp, freq, phase) in enumerate(freqlist[:, 1:4]):
        all_params.add_many(
            (
                "amp" + str(i),
                amp,
                True,
                0.5 * amp,
                1.5 * amp,
                None,
                None,
            ),
            (
                "freq" + str(i),
                freq,
                True,
                freq - fres,
                freq + fres,
                None,
                None,
            ),
            ("phase" + str(i), phase, True, 0, 1, None, None),
        )

    lc_copy = lc.copy()
    lc_copy[:, 1] -= np.mean(lc_copy[:, 1])

    out = minimize(
        _residual_multi,
        all_params,
        args=(lc_copy[:, 0],),
        kws={"data": lc_copy[:, 1]},
        max_nfev=2000 * (nfreq + 1),
    )
    logger.write(fit_report(out))

    params_dict = out.params.valuesdict()
    amps = [v for k, v in params_dict.items() if "amp" in k]
    freqs = [v for k, v in params_dict.items() if "freq" in k]
    phases = [v for k, v in params_dict.items() if "phase" in k]

    try:
        params_std = np.sqrt(np.diag(out.covar)).reshape((nfreq, 3))
    except AttributeError:
        params_std = np.full((nfreq, 3), np.nan)
    snr = freqlist[:, 4]
    freqlist_ = np.column_stack(
        (range(1, nfreq + 1), amps, freqs, phases, snr, params_std)
    )

    # Step 4: Compute the amplitude criterion for all kept frequencies
    logger.write(
        "\n>> Step 4: Compute the amplitude criterion for all kept frequencies\n"
    )
    amp_criterion = get_amp_criterion(lc, freqlist_)
    freqlist_ = np.column_stack((freqlist_, amp_criterion))

    warn_about_freqs = freqlist_[
        (freqlist_[:, 8] < WARN_AMP_CRIT) | (freqlist_[:, 8] > 1 / WARN_AMP_CRIT)
    ]
    if len(warn_about_freqs):
        logger.write(
            f"Warning: Amplitude criterion is NOT between {WARN_AMP_CRIT:.2f} and "
            f"{1/WARN_AMP_CRIT:.2f} for these frequencies:"
        )
        for freq in warn_about_freqs:
            logger.write(int(freq[0]), freq[2], freq[8])
    else:
        logger.write(
            f"Amplitude criterion is between {WARN_AMP_CRIT:.2f} and "
            f"{1/WARN_AMP_CRIT:.2f} for all extracted frequencies."
        )

    # Pickle global fitted model, if needed
    if pickle_model is not None:
        if not isinstance(pickle_model, str):
            pickle_model = "model.pickle"
        with open(f"data/processed/{pickle_model}", "wb") as f:
            pickle.dump(out, f)

    return freqlist_, out


def get_spectral_win(lc):
    """Compute the spectral window of the lightcurve."""
    N = len(lc)
    ls = LombScargle(
        lc[:, 0],
        np.sin(2 * np.pi * lc[:, 0]),
        normalization="psd",
        fit_mean=False,
        center_data=False,
    )
    freq, power = ls.autopower(samples_per_peak=10, nyquist_factor=1)
    return np.column_stack((freq, 2 * np.sqrt(power / N)))


def _sine_func(t, *p):
    """ """
    amp, freq, phase = p
    return amp * np.sin(2 * np.pi * (freq * t + phase))


def _lightcurve_func(t, *p):
    """ """
    offset = p[0]
    nfreq = len(p) // 3
    p = np.array(p[1:]).reshape((nfreq, 3))
    lc_model = np.zeros(len(t))
    for i in range(nfreq):
        lc_model += _sine_func(t, *p[i, :])
    lc_model += offset
    return lc_model


def discard_unresolved_peaks(freqlist, fres, logger):
    """ """
    nfreq = len(freqlist)
    to_keep = [True] * nfreq
    for i in range(nfreq - 1, 0, -1):
        # Is this an unresolved peak?
        unresolved = np.abs(freqlist[i, 2] - freqlist[:i, 2]) < RES_FACTOR * fres
        to_keep[i] = not np.any(unresolved)

    n_unresolved = nfreq - len(freqlist[to_keep])
    if n_unresolved > 1:
        logger.write(f"{n_unresolved} unresolved peaks were discarded:")
    elif n_unresolved == 1:
        logger.write(f"{n_unresolved} unresolved peak was discarded:")
    else:
        logger.write("No unresolved peaks.")
    for freq in freqlist[[not b for b in to_keep]]:
        logger.write(int(freq[0]), freq[1], freq[2], freq[3])

    return freqlist[to_keep]


def fit_lc_model(lc, freqlist, offset):
    """ """
    p0 = np.insert(freqlist[:, 1:4].flatten(), 0, offset)
    return curve_fit(_lightcurve_func, lc[:, 0], lc[:, 1], p0=p0)


def get_amp_criterion(lc, freqlist):
    """ """
    ls = LombScargle(lc[:, 0], lc[:, 1])
    amp_criterion = np.zeros(len(freqlist))
    for (i, f) in enumerate(freqlist[:, 2]):
        model_param = ls.model_parameters(f)
        amp_criterion[i] = freqlist[i, 1] / np.sqrt(
            model_param[1] ** 2 + model_param[2] ** 2
        )
    return amp_criterion


def identify_combinations(freqlist, comb_factor=2, nparents=20, thresh_tol=EPS_COMBI):
    """ """
    # TODO: Dirty code: there should be a better way to do this
    # Get all possible coefficient combinations
    nparents = min(len(freqlist), nparents)

    pos_coeff_combs = []
    for coeffs in product(range(-comb_factor + 1, comb_factor + 1), repeat=comb_factor):
        if 1 < sum([abs(i) for i in coeffs]) <= comb_factor and not np.all(
            np.array(coeffs) < 0
        ):
            pos_coeff_combs.append(coeffs)

    identified_combinations = []
    for freq_ids in combinations(range(1, nparents + 1), comb_factor):
        freq_ids = np.array(freq_ids, dtype=int)
        for coeffs in pos_coeff_combs:
            mask_comb = (
                np.abs(freqlist[:, 2] - np.dot(coeffs, freqlist[freq_ids - 1, 2]))
                < thresh_tol
            )
            if np.any(mask_comb):
                coeffs_extended = np.zeros(nparents, dtype=int)
                coeffs_extended[freq_ids - 1] = list(coeffs)
                for i in freqlist[mask_comb, 0].astype(int):
                    identified_combinations.append(
                        [i]
                        + [
                            freqlist[i - 1, 2],
                            np.dot(coeffs, freqlist[freq_ids - 1, 2]),
                        ]
                        + coeffs_extended.tolist()
                    )
    identified_combinations = np.array(identified_combinations)
    # Drop duplicates
    _, unique_combs = np.unique(identified_combinations, return_index=True, axis=0)
    identified_combinations = identified_combinations[unique_combs]
    if len(identified_combinations):
        # Drop lines where the combination frequency is in the combination expression
        columns = identified_combinations[identified_combinations[:, 0] <= nparents, 0]
        rows = np.arange(len(columns), dtype=int)[:, np.newaxis]
        columns = (columns.astype(int) + 2)[:, np.newaxis]
        to_keep = np.ones(len(identified_combinations), dtype=bool)
        to_keep[: len(rows)] = (identified_combinations[rows, columns] == 0).ravel()
        return identified_combinations[to_keep]


def _residual(params, t, data=None):
    model = params["amp"] * np.sin(2 * np.pi * (params["freq"] * t + params["phase"]))
    if data is None:
        return model
    return model - data


def _residual_multi(params, t, data=None):
    params_dict = params.valuesdict()
    amps = [v for k, v in params_dict.items() if "amp" in k]
    freqs = [v for k, v in params_dict.items() if "freq" in k]
    phases = [v for k, v in params_dict.items() if "phase" in k]
    model = 0
    for amp, freq, phase in zip(amps, freqs, phases):
        model += amp * np.sin(2 * np.pi * (freq * t + phase))
    if data is None:
        return model
    return model - data


if __name__ == "__main__":
    # rng = np.random.RandomState(1)
    # t = np.linspace(0,100,10000)
    # y = 1.2 + 5 * np.sin(2*np.pi*0.5*t + 0.8*np.pi) + 2*rng.randn(len(t)) + \
    #     5*np.sin(2*np.pi*0.53*t + 1.5*np.pi)
    # lc = np.column_stack((t,y))
    # freqlist = extract_freqs(lc)
    # p = np.insert(freqlist[:,1:4].flatten(),0,1.2)
    # model = _lightcurve_func(t,*p)

    # example_freqlist = 2*rng.rand(20)
    # example_freqlist = np.concatenate((example_freqlist,2*example_freqlist))
    # example_freqlist = np.column_stack((np.arange(1,len(example_freqlist)+1),example_freqlist))
    # combinations_ = identify_combinations(example_freqlist)
    import src.frequency_analysis.prepare

    lc = src.frequency_analysis.prepare.prepare(102603266)
    freqlist, out = extract_freqs(lc)
    combi_freqs = identify_combinations(freqlist)
    # import src.frequency_analysis.save
    # import src.frequency_analysis.plot

    # src.frequency_analysis.save.save_all("16.hdf5", lc, out, freqlist)
    # src.frequency_analysis.plot.plot_all("data/processed/16.hdf5", "16")
    # lc = src.prepare.prepare("102603266")
    # freqlist2, freqlist3 = extract_freqs_with_curve_fit(lc)
    # freqlist_ = np.genfromtxt("102603266_freqlist.txt")
    # combinations = identify_combinations(freqlist_[:,:])
