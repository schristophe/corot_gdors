import glob
import os
import numpy as np
from astropy.stats import sigma_clip
from astropy.table import Table
from scipy.signal import savgol_filter


def prepare(star_id, get_errors=False):
    """ """
    filename = glob.glob(f"data/raw/*{star_id:010d}*.fits")
    print(filename)
    if "AN2" in filename[0]:
        barreg = Table.read(filename[0], hdu=3)
        df = barreg["DATEBARREGTT", "FLUXBARREG", "FLUXDEVBARREG"].to_pandas()
        print(df.columns)
        # Flatten long trends
        window_len = int(1 / (0.2 * np.median(np.diff(barreg["DATEBARREGTT"]))))
        trends = savgol_filter(barreg["FLUXBARREG"], window_len, 2)
        df["FLUXBARREG_DETRENDED"] = df["FLUXBARREG"] / trends
        df["FLUXDEVBARREG_DETRENDED"] = df["FLUXDEVBARREG"] / trends

        if get_errors:
            return df[
                ["DATEBARREGTT", "FLUXBARREG_DETRENDED", "FLUXDEVBARREG_DETRENDED"]
            ].to_numpy()
        else:
            return df[["DATEBARREGTT", "FLUXBARREG_DETRENDED"]].to_numpy()

    elif "EN2" in filename[0]:
        barfil = Table.read(filename[0], hdu=2)
        df = barfil["DATEBARTT", "WHITEFLUXFIL"].to_pandas()

        # Flatten long trends
        window_len = int(1 / (0.2 * np.median(np.diff(barfil["DATEBARTT"]))))
        trends = savgol_filter(barfil["WHITEFLUXFIL"], window_len, 2)
        df["WHITEFLUXFIL_DETRENDED"] = df["WHITEFLUXFIL"] / trends

        return df[["DATEBARTT", "WHITEFLUXFIL_DETRENDED"]].to_numpy()
