import matplotlib.pyplot as plt
import numpy as np
from astropy.table import Table

from src.frequency_analysis.extract import SNR_LIMIT, WINDOW_LENGTH

plt.style.use("ggplot")


def plot_all(datafile, filename):
    """"""
    plot_lc(datafile=datafile, filename=f"{filename}_lc.png")
    plot_lc_residuals(datafile=datafile, filename=f"{filename}_lc_residuals.png")
    plot_lsper(datafile=datafile, filename=f"{filename}_lsper.png")
    plot_lsper_residuals(datafile=datafile, filename=f"{filename}_lsper_residuals.png")
    plot_spectral_window(datafile=datafile, filename=f"{filename}_spectral_window.png")


def plot_lc(data=None, datafile=None, model=None, filename=None):
    """"""
    if datafile is not None:
        datafile = f"data/processed/{datafile}"
        lc = Table.read(datafile, path="lc")
        lc = np.array([lc["time"], lc["normalized_flux"]]).T
        try:
            lc_res = Table.read(datafile, path="lc_residuals")
            lc_res = np.array([lc_res["time"], lc_res["normalized_flux"]]).T
            model = lc[:, 1] - lc_res[:, 1]
        except OSError:
            lc_res = None
            model = None
    elif data is not None:
        lc = data
        lc_res = None
    else:
        raise NoDataProvidedError
    fig, axes = plt.subplots(2, 1, figsize=(15, 12))

    for ax in axes:
        ax.plot(lc[:, 0], lc[:, 1])
        ax.set(xlabel="Time", ylabel="Normalized Flux")
        if model is not None:
            ax.plot(lc[:, 0], model)

    # Zoom in where residuals are larger
    if lc_res is not None:
        t_worst = lc[np.argmax(np.abs(lc_res[:, 1])), 0]
        t_min = max(t_worst - 10, lc[0, 0])
        t_max = min(t_worst + 10, lc[-1, 0])
        axes[1].set_xlim(t_min, t_max)
    else:
        t_mid = (lc[0, 0] + lc[-1, 0]) / 2
        axes[1].set_xlim(t_mid - 10, t_mid + 10)

    if filename is not None:
        fig.savefig(f"data/processed/{filename}")
        plt.close(fig)
    else:
        plt.show()


def plot_lc_residuals(data=None, datafile=None, model=None, filename=None):
    """"""
    if datafile is not None:
        datafile = f"data/processed/{datafile}"
        try:
            lc_res = Table.read(datafile, path="lc_residuals")
        except OSError:
            print(
                "Warning: No residuals to plot. This usually happens when no "
                "significant frequency could be extracted from the lightcurve."
            )
            return
        lc_res = np.array([lc_res["time"], lc_res["normalized_flux"]]).T
    elif data is not None:
        lc_res = data
    else:
        raise NoDataProvidedError

    fig, ax = plt.figure(figsize=(15, 6)), plt.axes()
    ax.plot(lc_res[:, 0], lc_res[:, 1])
    ax.set(xlabel="Time", ylabel="Normalized Flux - Residuals")

    if filename is not None:
        fig.savefig(f"data/processed/{filename}")
        plt.close(fig)
    else:
        plt.show()


def plot_lsper(data=None, datafile=None, freqlist=None, filename=None):
    """"""
    if datafile is not None:
        datafile = f"data/processed/{datafile}"
        lsper = Table.read(datafile, path="lsper")
        lsper = np.array([lsper["frequency"], lsper["amplitude"]]).T
        try:
            freqlist = Table.read(datafile, path="freqlist")
            freqlist = freqlist["frequency"]
        except OSError:
            freqlist = None
    elif data is not None:
        lsper = data
    else:
        raise NoDataProvidedError
    fig, axes = plt.subplots(2, 1, figsize=(15, 12))
    xcut = 8
    icut = np.searchsorted(lsper[:, 0], xcut)
    # Low frequency region
    axes[0].set_xlim(0, xcut)
    axes[0].plot(lsper[:icut, 0], lsper[:icut, 1])
    # High frequency region
    axes[1].set_xlim(xcut, max(lsper[:, 0]))
    axes[1].plot(lsper[icut:, 0], lsper[icut:, 1])
    for ax in axes:
        ax.set(xlabel="Frequency (c/d)", ylabel="Amplitude")

    if freqlist is not None:
        for ax in axes:
            ymax = ax.get_ylim()[1]
            ax.vlines(freqlist, ymin=0.95 * ymax, ymax=ymax, colors="#424242")

    if filename is not None:
        fig.savefig(f"data/processed/{filename}")
        plt.close(fig)
    else:
        plt.show()


def plot_lsper_residuals(data=None, datafile=None, filename=None):
    """"""
    if datafile is not None:
        datafile = f"data/processed/{datafile}"
        try:
            lsper_res = Table.read(datafile, path="lsper_residuals")
        except OSError:
            print(
                "Warning: No residuals to plot. This usually happens when no "
                "significant frequency could be extracted from the lightcurve."
            )
            return
        lsper_res = np.array([lsper_res["frequency"], lsper_res["amplitude"]]).T
    elif data is not None:
        lsper_res = data
    else:
        raise NoDataProvidedError

    amp_lvl_at_snr_lim = _get_amplitude_level_at_snr_limit(lsper_res)
    fig, axes = plt.subplots(2, 1, figsize=(15, 12))
    xcut = 8
    icut = np.searchsorted(lsper_res[:, 0], xcut)
    # Low frequency region
    axes[0].set_xlim(0, xcut)
    axes[0].plot(lsper_res[:icut, 0], lsper_res[:icut, 1])
    axes[0].plot(amp_lvl_at_snr_lim[:, 0], amp_lvl_at_snr_lim[:, 1])
    # High frequency region
    axes[1].set_xlim(xcut, max(lsper_res[:, 0]))
    axes[1].plot(lsper_res[icut:, 0], lsper_res[icut:, 1])
    axes[1].plot(
        amp_lvl_at_snr_lim[icut // 1000 :, 0], amp_lvl_at_snr_lim[icut // 1000 :, 1]
    )

    for ax in axes:
        ax.set(xlabel="Frequency (c/d)", ylabel="Amplitude")

    if filename is not None:
        fig.savefig(f"data/processed/{filename}")
        plt.close(fig)
    else:
        plt.show()


def plot_spectral_window(data=None, datafile=None, filename=None):
    """ """
    if datafile is not None:
        datafile = f"data/processed/{datafile}"
        window = Table.read(datafile, path="spectral_window")
        window = np.array([window["frequency"], window["amplitude"]]).T
    elif data is not None:
        window = data
    else:
        raise NoDataProvidedError

    fig, ax = plt.figure(figsize=(15, 6)), plt.axes()
    ax.plot(window[:, 0], window[:, 1])
    ax.set(xlim=(0, 2), xlabel="Frequency (c/d)", ylabel="Amplitude")
    if filename is not None:
        fig.savefig(f"data/processed/{filename}")
        plt.close(fig)
    else:
        plt.show()


def _get_amplitude_level_at_snr_limit(lsper):
    """ """
    amp_lvl_at_snr_lim = []
    for freq in lsper[::1000, 0]:
        mask_window = (lsper[:, 0] >= freq - 0.5 * WINDOW_LENGTH) & (
            lsper[:, 0] <= freq + 0.5 * WINDOW_LENGTH
        )
        amp_lvl_at_snr_lim.append([freq, SNR_LIMIT * np.mean(lsper[mask_window, 1])])
    return np.array(amp_lvl_at_snr_lim)


class NoDataProvidedError(Exception):
    pass
