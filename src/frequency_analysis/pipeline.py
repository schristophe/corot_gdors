import src.frequency_analysis.download
import src.frequency_analysis.prepare
import src.frequency_analysis.extract
import src.frequency_analysis.save
import src.frequency_analysis.plot


def process_batch(star_ids):
    """"""
    # Download all necessary data
    src.frequency_analysis.download.download(star_ids)
    # Process each star individually
    for id in star_ids:
        process_star(id)


def process_star(star_id):
    """ """
    lc = src.frequency_analysis.prepare.prepare(star_id)
    if lc is not None:
        freqlist, out = src.frequency_analysis.extract.extract_freqs(
            lc, log=f"{int(star_id)}.log"
        )
        src.frequency_analysis.save.save_all(f"{int(star_id)}.hdf5", lc, out, freqlist)
        src.frequency_analysis.plot.plot_all(f"{int(star_id)}.hdf5", f"{star_id}")


if __name__ == "__main__":
    process_batch([102590420])
