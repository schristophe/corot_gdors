import os.path

import requests
from astroquery.vizier import Vizier


def download(star_ids):
    """Download N2 CoRoT data products from Vizier."""
    if type(star_ids) in [int, float, str]:
        star_ids = [star_ids]
    star_ids = [int(id) for id in star_ids]

    # Use CoRoT catalog on Vizier to get the run ID for each target
    query = ",".join([str(id) for id in star_ids])
    result = Vizier(catalog="B/corot", columns=["CoRoT", "FileName"]).query_constraints(
        CoRoT=query
    )

    if len(result) == 0:
        print("None of the provided ID(s) were found in the CoRoT catalog.")
    else:
        # For each target, download data in fits format
        for table in result:
            for (id, filename) in table:
                if os.path.isfile(f"data/raw/{filename.split('/')[-1] }"):
                    print(id, "IN CACHE")
                else:
                    r = requests.get(
                        f"http://vizier.u-strasbg.fr/viz-bin/nph-Cat?-plus=-%2b&B/corot/files/{filename}"
                    )
                    if r.status_code == 200:
                        with open(f"data/raw/{ filename.split('/')[-1] }", "wb") as f:
                            f.write(r.content)
                        print(id, "OK")
                    else:
                        print(id, f"Failed (code: {r.status_code})")


if __name__ == "__main__":
    download([102603266, 116])
