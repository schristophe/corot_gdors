class Logger(object):
    """"""

    def __init__(self, log=None):
        """"""
        self.log = log

    def write(self, *args):
        """"""
        if self.log is None:
            print(*args)
        else:
            if not isinstance(self.log, str):
                self.log = "extract_freqs.log"
            with open(f"data/processed/{self.log}", "a") as f:
                for w in args[:-1]:
                    f.write(f"{w} ")
                f.write(f"{args[-1]}\n")
