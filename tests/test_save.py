import numpy as np

import src.frequency_analysis.save


def test_save_all(mocker):
    mock_lc = mocker.patch("src.frequency_analysis.save._save_lc")
    mock_lc_res = mocker.patch("src.frequency_analysis.save._save_lc_residuals")
    mock_lsper = mocker.patch("src.frequency_analysis.save._save_lsper")
    mock_lsper_res = mocker.patch("src.frequency_analysis.save._save_lsper_residuals")
    mock_spectral_window = mocker.patch(
        "src.frequency_analysis.save._save_spectral_window"
    )
    mock_freqlist = mocker.patch("src.frequency_analysis.save._save_freqlist")
    mocker_combination_list = mocker.patch(
        "src.frequency_analysis.save._save_combination_list"
    )
    src.frequency_analysis.save.save_all("", "", "", np.array([1]))
    mock_lc.assert_called_once()
    mock_lc_res.assert_called_once()
    mock_lsper.assert_called_once()
    mock_lsper_res.assert_called_once()
    mock_spectral_window.assert_called_once()
    mock_freqlist.assert_called_once()
    mocker_combination_list.assert_called_once()


def test_save_all_no_frequency(mocker):
    mock_lc = mocker.patch("src.frequency_analysis.save._save_lc")
    mock_lc_res = mocker.patch("src.frequency_analysis.save._save_lc_residuals")
    mock_lsper = mocker.patch("src.frequency_analysis.save._save_lsper")
    mock_lsper_res = mocker.patch("src.frequency_analysis.save._save_lsper_residuals")
    mock_spectral_window = mocker.patch(
        "src.frequency_analysis.save._save_spectral_window"
    )
    mock_freqlist = mocker.patch("src.frequency_analysis.save._save_freqlist")
    mocker_combination_list = mocker.patch(
        "src.frequency_analysis.save._save_combination_list"
    )
    src.frequency_analysis.save.save_all("", "", "", np.array([]))
    mock_lc.assert_called_once()
    mock_lsper.assert_called_once()
    mock_spectral_window.assert_called_once()
    mock_lc_res.assert_not_called()
    mock_lsper_res.assert_not_called()
    mock_freqlist.assert_not_called()
    mocker_combination_list.assert_not_called()
