import pytest

from src.frequency_analysis import plot


def test_plot_all(mocker):
    mock_lc = mocker.patch("src.frequency_analysis.plot.plot_lc")
    mock_lc_res = mocker.patch("src.frequency_analysis.plot.plot_lc_residuals")
    mock_lsper = mocker.patch("src.frequency_analysis.plot.plot_lsper")
    mock_lsper_res = mocker.patch("src.frequency_analysis.plot.plot_lsper_residuals")
    mock_spectral_window = mocker.patch(
        "src.frequency_analysis.plot.plot_spectral_window"
    )
    plot.plot_all("", "")
    mock_lc.assert_called_once()
    mock_lc_res.assert_called_once()
    mock_lsper.assert_called_once()
    mock_lsper_res.assert_called_once()
    mock_spectral_window.assert_called_once()


def test_plot_lc_no_data_provided():
    with pytest.raises(plot.NoDataProvidedError):
        plot.plot_lc()


def test_plot_lc_residuals_no_data_provided():
    with pytest.raises(plot.NoDataProvidedError):
        plot.plot_lc_residuals()


def test_plot_lsper_no_data_provided():
    with pytest.raises(plot.NoDataProvidedError):
        plot.plot_lsper()


def test_plot_lsper_residuals_no_data_provided():
    with pytest.raises(plot.NoDataProvidedError):
        plot.plot_lsper_residuals()


def test_plot_spectral_window_no_data_provided():
    with pytest.raises(plot.NoDataProvidedError):
        plot.plot_spectral_window()
