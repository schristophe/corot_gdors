import numpy as np

import src.frequency_analysis.extract as extract

# identify_combinations()
# Test when :
# - no frequency where extracted
# - nparents > number of extracted frequencies
# - no combination frequencies can be found
# - one parent frequency is a combination of itself and another frequency
# - only one frequency is a combination
# - many combination frequencies
# - use non-default parameters


def test_identify_combinations_no_frequency_extracted():
    identified_combinations = extract.identify_combinations(np.array([]))
    assert len(identified_combinations) == 0
